﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;

namespace WCF_Service_Host
{
    
    [ServiceContract (Namespace="http://Main/WCFHost/Awesome", ProtectionLevel =System.Net.Security.ProtectionLevel.EncryptAndSign)]
    public interface IAwesomeService
    {
        [FaultContract(typeof (BadBeans))]
        [OperationContract(Action ="CoolBeans",ProtectionLevel =System.Net.Security.ProtectionLevel.EncryptAndSign)]
        CoolBeans DoWork();


        [OperationContract(Action = "SendMessage")]
        void AnonMessage(string  message);
    }

    [DataContract(Namespace ="BARBEQUE")]
    public class CoolBeans
    {
        [DataMember(Order =0, Name = "0x3B9AC9FF")]
        public string Advice { get; set; }
            
        [DataMember(Order =1,  Name = "0x2E5BF271")]
        public string Cheer { get; set; }

        [OnSerializing]
        void OnSerializing( StreamingContext ctx)
        { Advice = "Trust yourself"; Cheer = "You're great"; }


    }

    [DataContract]
    public class BadBeans
    {
        [DataMember(Order = 0, Name = "BEANS!")]
        public string Jeer { get { return "TOO MANY BEANS"; }  set { this.Jeer = value; } }

    }


    [ErrorHandler]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AwesomeService" in both code and config file together.
    public class AwesomeService : IAwesomeService
    {
        [OperationBehavior(Impersonation = ImpersonationOption.Required)]
        public CoolBeans DoWork()
        {
            if (System.Threading.Thread.CurrentPrincipal.IsInRole("Administrators"))
            { Console.Beep(432, 40); System.Threading.Thread.Sleep(100); Console.Beep(432, 40); }
            WindowsIdentity id = System.Threading.Thread.CurrentPrincipal.Identity as WindowsIdentity;
            if (id == null)
            {
                throw new FaultException(new FaultReason("Windows Authentication is required"));
            }
            else
            {
                using (WindowsImpersonationContext wic = id.Impersonate())
                {
                    return new CoolBeans();
                }
            }
        }

        [TransactionFlow(TransactionFlowOption.Allowed)]
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        void IAwesomeService.AnonMessage(string message)
        {
            AuthorizationBehavior2 ab2 = new AuthorizationBehavior2();
            OperationContext ctx = OperationContext.Current;

            ab2.CheckAccess(ctx);

            Console.WriteLine(message);

        }
    }
}
