﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Service_Host
{
    [ServiceContract(Namespace = "http://Main/WCFHost/Chat")]
    public interface IChat
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(ChatMessage msg);

    }
    [ServiceContract(Namespace = "http://Main/WCFHost/Chat",CallbackContract=typeof(IChat)) ]
    public interface IChatManager
    {
        [OperationContract(IsOneWay = true)]
        void RegisterClient(string name);

        [OperationContract(IsOneWay = true)]
        void SubmitMessage(ChatMessage msg);
    }

    [DataContract(Namespace = "http://Main/WCFHost/Chat")]
    public class ChatMessage
    {
        public ChatMessage() { }
        public ChatMessage(string n, DateTime ts, string m)
        {
            this.Name = n;
            this.TimeStamp = ts;
            this.Message = m;
        }

        [DataMember]
        public string Name;

        [DataMember]
        public DateTime TimeStamp;

        [DataMember]
        public string Message;

    }

    public class ChatService : IChat
    {
        public void SendMessage(ChatMessage msg)
        {
            Console.WriteLine("{0} {1} says {2}", msg.TimeStamp, msg.Name, msg.Message);
        }
    }

    [ServiceBehavior(InstanceContextMode =InstanceContextMode.Single, IncludeExceptionDetailInFaults =true)]
    public class ChatManagerService : IChatManager
    {
        Dictionary<string, IChat> Clients = new Dictionary<string, IChat>();

        void IChatManager.RegisterClient(string name)
        {

            IChat client = OperationContext.Current.GetCallbackChannel<IChat>();
            if (client != null)
            {
                Clients.Add(name, client);
                Console.WriteLine("{0} joined", name);
            }
        }

        void IChatManager.SubmitMessage(ChatMessage msg)
        {
            foreach (string key in Clients.Keys)
            {
                try
                {

                    if (!msg.Name.Equals(key))
                        Clients[key].SendMessage(msg);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        
        }
    }

}
