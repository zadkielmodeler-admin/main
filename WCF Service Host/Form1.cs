﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WCF_Service_Host
{
    public partial class Form1 : Form
    {
        ServiceBase AwesomeHost;
        readonly string AwesomeServiceName = "AwesomeService";
        public Form1()
        {
            InitializeComponent();
            ServiceListBox.Items.Add(AwesomeServiceName);
            MessageBox.Show(StartService().ToString());

        }

        private void StopButton_Click(object sender, EventArgs e)
        {


        }

        private void StartButton_Click(object sender, EventArgs e)
        {

        }

        private void ChangeButton_Click(object sender, EventArgs e)
        {

        }

        private void ServiceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ServiceListBox.SelectedIndex == -1)
            {
                StopButton.Enabled = false;
                StartButton.Enabled = false;
                ChangeButton.Enabled = false;
            }
            else
            {
                StopButton.Enabled = true;
                StartButton.Enabled = true;
                ChangeButton.Enabled = true;

            }



        }

        private bool StartService()
        {


            AwesomeHost = new ServiceBase(typeof(AwesomeService));
            return true;


        }
    }



}
