﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Service_Host
{
    class Authorization
    {
    }

    class AuthorizationBehavior2 : ServiceAuthorizationManager
    {
        /// <summary>
        /// Override check access to ONLY allow anonymous authentication.
        /// </summary>
        /// <param name="operationContext"></param>
        /// <returns> bool</returns>
        public override bool CheckAccess(OperationContext operationContext)
        {
            ServiceSecurityContext ctx = operationContext.ServiceSecurityContext;
            foreach (ClaimSet cs in ctx.AuthorizationContext.ClaimSets)
            {
                foreach (Claim c in cs)
                {
                    if (c.ClaimType.Equals(ClaimTypes.Anonymous))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
