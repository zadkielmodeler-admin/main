﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace WCF_Service_Host
{
    //https://msdn.microsoft.com/en-us/library/bb412179(v=vs.110).aspx
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBigMathService" in both code and config file together.
    [ServiceContract (Namespace="http://Main/WCFHost/Math")]
 
    public interface IBigMathService
    {
        //Binary commands
        [WebInvoke(UriTemplate ="Binary/Add/{first}/{second}",Method ="POST", ResponseFormat =WebMessageFormat.Json)]
        BigInteger Add(BigInteger first, BigInteger second);

        [WebInvoke(UriTemplate = "Binary/Subtract/{first}/{second}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Subtract(BigInteger first, BigInteger second);

        [WebInvoke(UriTemplate = "Binary/Multiply/{first}/{second}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Mutliply(BigInteger first, BigInteger second);

        [WebInvoke(UriTemplate = "Binary/Divide/{first}/{second}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Divide(BigInteger first, BigInteger second);

        [WebInvoke(UriTemplate = "Binary/Remainder/{first}/{second}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Remainder(BigInteger first, BigInteger second);

        [WebInvoke(UriTemplate = "Binary/POW/{first}/{second}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger POW(BigInteger first, int second);



        //unary commands
        [WebInvoke(UriTemplate = "Unary/LogE/{first}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger LogE(BigInteger first);

        [WebInvoke(UriTemplate = "Unary/Log10/{first}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Log10(BigInteger first);

        [WebInvoke(UriTemplate = "Unary/Negate/{first}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger Negate(BigInteger first);

        [WebInvoke(UriTemplate = "Unary/Absolute/{first}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger AbsoluteValue(BigInteger first);
        //Nilakantha


        [WebInvoke(UriTemplate = "Unary/PI/{Precision}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger NilakanthaPI(BigInteger Precision);

        [WebInvoke(UriTemplate = "Unary/SinePI/{Precision}/", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        BigInteger SinePI(BigInteger Precision);  // calculate pi: x * sin(180 / x)  larger is better
                                                  //http://en.wikipedia.org/wiki/Taylor_series
                                                  // (x^3/3!)   +   (x^5/5!) - (x^7/7!)

        //trinary commands


        //paramterless commands

        [OperationContract (Action="*", ReplyAction ="*")]
        Message CatchAll(Message request);
    }

    [ErrorHandler]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BigMathService" in both code and config file together.
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class BigMathService //: IBigMathService
    {
        [PrincipalPermission(SecurityAction.Demand,Role ="Administrators") ]
        void stuff()
        {

            //BigInteger.
            
        }

        public Message CatchAll(Message request)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(request.GetReaderAtBodyContents());
            Console.WriteLine(doc.InnerXml);
            throw new FaultException("You called an unsupport Operation");
            return request;
        }
    }  

}
