﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Service_Host
{
    class ErrorHandler : IErrorHandler
    {
        bool IErrorHandler.HandleError(Exception error)
        {

            Console.WriteLine("An error was handled. Feeling dry. Going in some water on low heat for a few hours.");
            return true;

        }

        void IErrorHandler.ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            var fc = FaultCode.CreateSenderFaultCode("BadBeans", "http://Main/WCFHost/Awesome");
            fault = Message.CreateMessage(version, fc, "BadBreaneans", "da", "http://Main/WCFHost/Awesome/BADBEANS/BADBREANS/BAD/BREANS/BadBeans/StirThePot/TooManyBreans/SimmerTooLong/BadBeans");
        }
    }

    public class ErrorHandlerAttribute : Attribute, IServiceBehavior
    {
        void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
            foreach(ChannelDispatcher cd
                in serviceHostBase.ChannelDispatchers)
            {
                cd.ErrorHandlers.Add(new ErrorHandler());
            }
        }

        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
     
        }

        void IServiceBehavior.Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
         
        }
    }
}
