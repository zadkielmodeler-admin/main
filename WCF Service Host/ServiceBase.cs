﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WCF_Service_Host
{

    public class ServiceBase
    {

        public string BaseAddress
        {
            get
            {
                return host.BaseAddresses[0].ToString();
            }
            set
            {
                Continue = false;
                Thread.Sleep(50); //give it 50 miliseconds to shut down the host on the other thread.
                Initialize(new Uri(value), type);
            }

        }
        private readonly Type type;
        private ServiceHost host;
        private Thread HostThread;
        private bool Continue = true;
        private ServiceEndpoint serviceEndpoint;

        /// <summary>
        /// Runs the host in it's own thread
        /// </summary>
        private void ServiceLoop()
        {
            host.Open();
            Console.WriteLine("The service is ready at {0}", this.BaseAddress);
            Console.WriteLine("Press <Enter> to stop the service.");
            Console.ReadLine();
        
            while (Continue)
            {

            }

            host.Close();
            (host as IDisposable).Dispose();
            HostThread.Join();
            HostThread = null;
        }

        public ServiceBase(Type t)
        {
            type = t;
            Uri baseAddress = new Uri("http://Main/WCFHost/");

            Initialize(baseAddress, t);

        }

        private void Initialize(Uri address, Type t)
        {
            Continue = true;

            Uri[] addresses = new Uri[1];
            addresses[0] = address;

            host = new ServiceHost(t, addresses);


            ////need to do this configurably via class properties (getters and setters)
            //WSHttpBinding bs = new WSHttpBinding();
            //bs.ReliableSession.Enabled = true;
            //bs.ReliableSession.Ordered = true;
            //bs.TransactionFlow = true;

            //NetTcpBinding ntcp = new NetTcpBinding();
            //ntcp.ReliableSession.Enabled = true;
            //ntcp.ReliableSession.Ordered = true;
            //ntcp.ReliableSession.InactivityTimeout = new TimeSpan(20000);


            ////no reliable session
            //NetNamedPipeBinding nnps = new NetNamedPipeBinding();

            ////no reliable session
            //NetMsmqBinding nmsmq = new NetMsmqBinding();

            // Enable metadata publishing.
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            host.Description.Behaviors.Add(smb);

            //add the mex endpoint programmatically on a per Service Contract basis
            host.AddServiceEndpoint(
            typeof(IMetadataExchange),
            MetadataExchangeBindings.CreateMexHttpsBinding(),
            "http://Main/WCFHost/mex/" + type.ToString());


            HostThread = new Thread(new ThreadStart(ServiceLoop));

        }
        //Not sure if these work as  the host is open and running on a separate thread.
        public bool Open()
        {
            try
            {   //host.Open():
                Initialize(new Uri(this.BaseAddress), this.type);
                return true;
            }
            catch { return false; }
        }

        public bool Close()
        {
            try
            {

                Continue = false;
               

                return true;
            }
            catch { return false; }

        }
        //will have to do more boiler plate code here;
        public bool ReliableEnabled
        {
            get
            {
                if (EndPoint == EndPointType.DualHTTP)
                {
                    return true;

                }
                else if (EndPoint == EndPointType.TCP)
                {
                    return (serviceEndpoint.Binding as NetTcpBinding).ReliableSession.Enabled;
                }
                else
                {
                    return false;
                }
            }
            set
            {

                if (EndPoint == EndPointType.DualHTTP)
                {
                    //do nothing it is enabled by default
                }
                else if (EndPoint == EndPointType.TCP)
                {
                    (serviceEndpoint.Binding as NetTcpBinding).ReliableSession.Enabled = value;
                }
                else
                {
                    //do nothing it can't be enabled in any other scenario
                }
            }
        }
             

        public bool ReliableOrdered
        {
            get

            {
                if (EndPoint == EndPointType.DualHTTP)
                {
                    return (serviceEndpoint.Binding as WSDualHttpBinding).ReliableSession.Ordered;

                }
                else if (EndPoint == EndPointType.TCP)
                {
                    return (serviceEndpoint.Binding as NetTcpBinding).ReliableSession.Ordered;
                }
                else
                {

                    return false;
                }


            }
            set
            {

                if (EndPoint == EndPointType.DualHTTP)
                {
                    (serviceEndpoint.Binding as NetTcpBinding).ReliableSession.Ordered = value;
                }
                else if (EndPoint == EndPointType.TCP)
                {
                    (serviceEndpoint.Binding as NetTcpBinding).ReliableSession.Ordered = value;
                }
                else
                {
                    //do nothing it can't be enabled in any other scenario

                }


            }

        }

        /// <summary>
        /// Change the EndPoint type and restart the service host
        /// </summary>
        public EndPointType EndPoint {get { return this.EndPoint; }
            set
            {

                Continue = false;
                Thread.Sleep(50);
                Initialize(new Uri(BaseAddress), type);
                switch (value)
                {
                    case EndPointType.DualHTTP:
                        host.AddServiceEndpoint(type, new WSDualHttpBinding() , "http://Main/WCFHost/" + type.ToString() + "/" + value.ToString());
                        break;
                    case EndPointType.Rest:
                        WebHttpBinding web = new WebHttpBinding();
                        ServiceEndpoint endpoint = host.AddServiceEndpoint(type, new WebHttpBinding(), "http://Main/WCFHost/" + type.ToString() + "/" + value.ToString());
                        IEndpointBehavior  webBeh= new WebHttpBehavior();
                        endpoint.EndpointBehaviors.Add(webBeh);
                 
                 
                        break;
                    case EndPointType.TCP:
                        host.AddServiceEndpoint(type, new NetHttpBinding() , "net.tcp://Main/WCFHost/" + type.ToString() + "/" + value.ToString());
                        break;
                    case EndPointType.MSMQ:
                        host.AddServiceEndpoint(type, new NetMsmqBinding() , "net.msmq://Main/WCFHost/" + type.ToString() + "/" + value.ToString());
                        break;
    
                 }
            }

        }

    }

   public enum EndPointType
    {
        Rest,
        DualHTTP,
        MSMQ,
        TCP,

    }

}
