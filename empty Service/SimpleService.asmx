﻿<%@ WebService Language="C#" CodeBehind="SimpleService.asmx.cs" Class="namespacer.SimpleService" %>

using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;


namespace namespacer
{
    [WebService(Namespace = "http://tempuri.org/")]
    [System.Web.Script.Services.ScriptService]
    class SimpleService : WebService
    {
        public SimpleService()
        { }

        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public string HelloWorldo()
        {
            string[] greetings = { "hello", "Hi there", "Heelo Worldo" };
            JavaScriptSerializer js = new JavaScriptSerializer();
            var serialized = js.Serialize(greetings);
        
            return serialized;
        }

    }
}
