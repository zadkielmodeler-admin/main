﻿namespace Main
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChangeThemeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.CPPThreadingButton = new System.Windows.Forms.Button();
            this.WCFHostButton = new System.Windows.Forms.Button();
            this.BrowserButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ChangeThemeButton
            // 
            this.ChangeThemeButton.Location = new System.Drawing.Point(457, 81);
            this.ChangeThemeButton.Name = "ChangeThemeButton";
            this.ChangeThemeButton.Size = new System.Drawing.Size(135, 29);
            this.ChangeThemeButton.TabIndex = 0;
            this.ChangeThemeButton.Text = "Change Theme";
            this.ChangeThemeButton.UseVisualStyleBackColor = true;
            this.ChangeThemeButton.Click += new System.EventHandler(this.ChangeThemeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Programs";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 272);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 46);
            this.button1.TabIndex = 0;
            this.button1.Text = "Launch A";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 211);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 46);
            this.button2.TabIndex = 0;
            this.button2.Text = "Launch A";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(25, 220);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(146, 46);
            this.button3.TabIndex = 0;
            this.button3.Text = "Launch A";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // CPPThreadingButton
            // 
            this.CPPThreadingButton.Location = new System.Drawing.Point(25, 168);
            this.CPPThreadingButton.Name = "CPPThreadingButton";
            this.CPPThreadingButton.Size = new System.Drawing.Size(220, 46);
            this.CPPThreadingButton.TabIndex = 0;
            this.CPPThreadingButton.Text = "Launch C++ Threading APP";
            this.CPPThreadingButton.UseVisualStyleBackColor = true;
            this.CPPThreadingButton.Click += new System.EventHandler(this.CPPThreadingButton_Click);
            // 
            // WCFHostButton
            // 
            this.WCFHostButton.Location = new System.Drawing.Point(25, 116);
            this.WCFHostButton.Name = "WCFHostButton";
            this.WCFHostButton.Size = new System.Drawing.Size(220, 46);
            this.WCFHostButton.TabIndex = 0;
            this.WCFHostButton.Text = "Launch WCF Service Host";
            this.WCFHostButton.UseVisualStyleBackColor = true;
            this.WCFHostButton.Click += new System.EventHandler(this.WCFHostButton_Click);
            // 
            // BrowserButton
            // 
            this.BrowserButton.Location = new System.Drawing.Point(25, 64);
            this.BrowserButton.Name = "BrowserButton";
            this.BrowserButton.Size = new System.Drawing.Size(220, 46);
            this.BrowserButton.TabIndex = 0;
            this.BrowserButton.Text = "Launch Browser";
            this.BrowserButton.UseVisualStyleBackColor = true;
            this.BrowserButton.Click += new System.EventHandler(this.BrowserButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(451, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Options";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 486);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BrowserButton);
            this.Controls.Add(this.WCFHostButton);
            this.Controls.Add(this.CPPThreadingButton);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ChangeThemeButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ChangeThemeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button CPPThreadingButton;
        private System.Windows.Forms.Button WCFHostButton;
        private System.Windows.Forms.Button BrowserButton;
        private System.Windows.Forms.Label label2;
    }
}

